﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculadoraMatrices
{
    class Basicas
    {
        public void Elegir()
        {
            Basicas Proyecto = new Basicas();
            Console.WriteLine("1. Deseas sumar?");
            Console.WriteLine("2. Deseas restar?");
            string opcion = Console.ReadLine();
            switch (opcion)
            {
                case "1":
                    Proyecto.suma(opcion);
                    break;
                case "2":
                    Proyecto.suma(opcion);
                    break;
            }
        }
        public void suma(string opcion)
        {
            
            Console.WriteLine("Elige la cantidad de filas");
            int F1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Elige la cantidad de columnas");
            int C1 = int.Parse(Console.ReadLine());
            double[,] M1 = new double[F1, C1];

            for (int i = 0; i < F1; i++)
            {
                for(int j = 0; j<C1; j++)
                {
                    Console.WriteLine("Ingresa el valor [" + i + "]" + " [" + j + "]");
                    M1[i, j] = double.Parse(Console.ReadLine());
                    Console.Clear();
                }
            }
            Console.WriteLine("Ahora ingresaras los valores de la segunda matriz");
            Console.WriteLine("Presionar ENTER para continuar");
            Console.ReadLine();
            double[,] M2 = new double[F1, C1];
            
            for (int i = 0; i < F1; i++)
            {
                for (int j = 0; j < C1; j++)
                {
                    Console.WriteLine("Ingresa el valor [" + i + "]" + " [" + j + "]");
                    M2[i, j] = double.Parse(Console.ReadLine());
                    Console.Clear();
                }
            }
            double[,] M3 = new double[F1, C1];
            if(opcion == "1")
            {
                for (int i = 0; i < F1; i++)
                {
                    for (int j = 0; j < C1; j++)
                    {
                        Console.Write(M1[i, j] + " ");
                    }
                    Console.WriteLine("");
                }
                Console.WriteLine("+");
                for (int i = 0; i < F1; i++)
                {
                    for (int j = 0; j < C1; j++)
                    {
                        Console.Write(M2[i, j] + " ");
                    }
                    Console.WriteLine("");
                }
                for (int i = 0; i < F1; i++)
                {
                    for (int j = 0; j < C1; j++)
                    {
                        M3[i, j] = M1[i, j] + M2[i, j];
                    }
                }
                Console.WriteLine("=");
                for (int i = 0; i < F1; i++)
                {
                    for (int j = 0; j < C1; j++)
                    {
                        Console.Write(M3[i, j] + " ");
                    }
                    Console.WriteLine("");
                }
            }
            if (opcion == "2")
            {
                for (int i = 0; i < F1; i++)
                {
                    for (int j = 0; j < C1; j++)
                    {
                        Console.Write(M1[i, j] + " ");
                    }
                    Console.WriteLine("");
                }
                Console.WriteLine("-");
                for (int i = 0; i < F1; i++)
                {
                    for (int j = 0; j < C1; j++)
                    {
                        Console.Write(M2[i, j] + " ");
                    }
                    Console.WriteLine("");
                }
                for (int i = 0; i < F1; i++)
                {
                    for (int j = 0; j < C1; j++)
                    {
                        M3[i, j] = M1[i, j] - M2[i, j];
                    }
                }
                Console.WriteLine("=");
                for (int i = 0; i < F1; i++)
                {
                    for (int j = 0; j < C1; j++)
                    {
                        Console.Write(M3[i, j] + " ");
                    }
                    Console.WriteLine("");
                }
            }
            
        }
    }
}
