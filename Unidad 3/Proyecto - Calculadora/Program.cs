﻿using System;

namespace CalculadoraMatrices
{
    class Program
    {
        static void Main(string[] args)
        {
            string opciones = null;


            Console.WriteLine("CALCULADORA MATRICIAL - VERSION 0.1.9");
            Console.WriteLine("Hecho por: Rafael Juárez - 03IDVA"); 
            Console.WriteLine("SELECCIONE UNA OPCION");
            Console.WriteLine("1. SUMA O RESTA");
            Console.WriteLine("2. MULTIPLICACION");
            Console.WriteLine("3. GAUSS");
            Console.WriteLine("4. GAUSS-JORDAN");
            Console.WriteLine("5. INVERSA / GAUSS-JORDAN");
            Console.WriteLine("6. DETERMINANTES - CRAMER");
            Console.WriteLine("7. SARRUS");
            Console.WriteLine("8. COFACTORES");
            Console.WriteLine("9. INVERSA - DETERMINANTES");
            opciones = Console.ReadLine();
            Console.Clear();
            switch (opciones)
            {
                case "1":
                    Basicas basicas = new Basicas();
                    basicas.Elegir();
                    break;
                case "2":
                    Multiplicacion multis = new Multiplicacion();
                    multis.multi();
                    break;
                case "3":
                    Gauss gaussi = new Gauss();
                    gaussi.gauss();
                    break;
                case "4":
                    GaussJordan gj = new GaussJordan();
                    gj.Gauss();
                    break;
                case "5":
                    InversaGJ inver = new InversaGJ();
                    inver.inversa();
                    break;
                case "6":
                    DeterminantesCramer cra = new DeterminantesCramer();
                    cra.determinantes();
                    break;
                case "7":
                    Sarrus sarr = new Sarrus();
                    sarr.sarrus();
                    break;
                case "8":
                    Cofactores cofa = new Cofactores();
                    cofa.cofactores();
                    break;
                case "9":
                    Inversa invers = new Inversa();
                    invers.inversa();
                    break;
            }

            Console.ReadLine();

        }
    }
}
