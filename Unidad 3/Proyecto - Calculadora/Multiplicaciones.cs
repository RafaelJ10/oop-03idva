﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculadoraMatrices
{
    class Multiplicacion
    {
        public void multi()
        {
            Console.WriteLine("Elige la cantidad de filas");
            int F1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Elige la cantidad de columnas");
            int C1 = int.Parse(Console.ReadLine());
            double[,] M1 = new double[F1, C1];


            for (int i = 0; i < F1; i++)
            {
                for (int j = 0; j < C1; j++)
                {
                    Console.WriteLine("Ingresa el valor [" + i + "]" + " [" + j + "]");
                    M1[i, j] = double.Parse(Console.ReadLine());
                    Console.Clear();
                }
            }
            Console.Clear();

            Console.WriteLine("Ingresa el tamaño de la segunda matriz\n" +
                "Presiona ENTER para continuar...");
            Console.ReadLine();
            Console.Clear();

            Console.WriteLine("Elige la cantidad de filas");
            int F2 = int.Parse(Console.ReadLine());
            Console.WriteLine("Elige la cantidad de columnas");
            int C2 = int.Parse(Console.ReadLine());
            double[,] M2 = new double[F2, C2];

            if (F1 == F2 && C1 == C2 || F1 != C2 && C1 == F2)
            {
                double[,] M3 = new double[F1, C1];

                for (int i = 0; i < F1; i++)
                {
                    for (int j = 0; j < C1; j++)
                    {
                        Console.WriteLine("Ingresa el valor [" + i + "]" + " [" + j + "]");
                        M2[i, j] = double.Parse(Console.ReadLine());
                        Console.Clear();
                    }
                }
                Console.Clear();
                for (int i = 0; i < F1; i++)
                {
                    for (int j = 0; j < C1; j++)
                    {
                        Console.Write(M1[i, j] + " ");
                    }
                    Console.WriteLine("");
                }
                Console.WriteLine("*");
                for (int i = 0; i < F1; i++)
                {
                    for (int j = 0; j < C1; j++)
                    {
                        Console.Write(M2[i, j] + " ");
                    }
                    Console.WriteLine("");
                }
                for (int i = 0; i < F1; i++)
                {
                    for (int j = 0; j < C1; j++)
                    {
                        for (int k = 0; k < F2; k++)
                        {
                            M3[i, j] += M1[i, k] * M2[k, j];
                        }
                        
                    }
                }
                Console.WriteLine("=");
                for (int i = 0; i < F1; i++)
                {
                    for (int j = 0; j < C1; j++)
                    {
                        Console.Write(M3[i, j] + " ");
                    }
                    Console.WriteLine("");
                }



            }





        }   
    }
}
