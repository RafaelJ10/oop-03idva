﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculadoraMatrices
{
    class Sarrus
    {
        public void sarrus()
        {
            Console.WriteLine(" X11 X12 X13 = T1 ");
            Console.WriteLine(" X21 X22 X23 = T2 ");
            Console.WriteLine(" X31 X32 X33 = T3 ");

            Console.WriteLine("Ingresa el valor de X11");
            double x11 = double.Parse(Console.ReadLine());
            Console.Clear();


            Console.WriteLine(x11 + " X12 X13 = T1 ");
            Console.WriteLine(" X21 X22 X23 = T2 ");
            Console.WriteLine(" X31 X32 X33 = T3 ");

            Console.WriteLine("Ingresa el valor de X12");
            double x12 = double.Parse(Console.ReadLine());
            Console.Clear();


            Console.WriteLine(x11 + "  " + x12 + " X13 = T1 ");
            Console.WriteLine(" X21 X22 X23 = T2 ");
            Console.WriteLine(" X31 X32 X33 = T3 ");

            Console.WriteLine("Ingresa el valor de X13");
            double x13 = double.Parse(Console.ReadLine());
            Console.Clear();


            Console.WriteLine(x11 + "  " + x12 + "  " + x13 + " = T1 ");
            Console.WriteLine(" X21 X22 X23 = T2 ");
            Console.WriteLine(" X31 X32 X33 = T3 ");

            Console.WriteLine("Ingresa el valor T1");
            double t1 = double.Parse(Console.ReadLine());
            Console.Clear();


            Console.WriteLine(x11 + "  " + x12 + "  " + x13 + " = " + t1);
            Console.WriteLine(" X21 X22 X23 = T2 ");
            Console.WriteLine(" X31 X32 X33 = T3 ");

            Console.WriteLine("Ingresa el valor X21");
            double x21 = double.Parse(Console.ReadLine());
            Console.Clear();


            Console.WriteLine(x11 + "  " + x12 + "  " + x13 + " = " + t1);
            Console.WriteLine(x21 + " X22 X23 = T2 ");
            Console.WriteLine(" X31 X32 X33 = T3 ");

            Console.WriteLine("Ingresa el valor X22");
            double x22 = double.Parse(Console.ReadLine());
            Console.Clear();


            Console.WriteLine(x11 + "  " + x12 + "  " + x13 + " = " + t1);
            Console.WriteLine(x21 + "  " + x22 + " X23 = T2 ");
            Console.WriteLine(" X31 X32 X33 = T3 ");

            Console.WriteLine("Ingresa el valor X23");
            double x23 = double.Parse(Console.ReadLine());
            Console.Clear();


            Console.WriteLine(x11 + "  " + x12 + "  " + x13 + " = " + t1);
            Console.WriteLine(x21 + "  " + x22 + "  " + x23 + " = T2 ");
            Console.WriteLine(" X31 X32 X33 = T3 ");

            Console.WriteLine("Ingresa el valor T2");
            double t2 = double.Parse(Console.ReadLine());
            Console.Clear();


            Console.WriteLine(x11 + "  " + x12 + "  " + x13 + " = " + t1);
            Console.WriteLine(x21 + "  " + x22 + "  " + x23 + " = " + t2);
            Console.WriteLine(" X31 X32 X33 = T3 ");

            Console.WriteLine("Ingresa el valor X31");
            double x31 = double.Parse(Console.ReadLine());
            Console.Clear();


            Console.WriteLine(x11 + "  " + x12 + "  " + x13 + " = " + t1);
            Console.WriteLine(x21 + "  " + x22 + "  " + x23 + " = " + t2);
            Console.WriteLine(x31 + " X32 X33 = T3 ");
            Console.WriteLine("Ingresa el valor X32");
            double x32 = double.Parse(Console.ReadLine());
            Console.Clear();

            Console.WriteLine(x11 + "  " + x12 + "  " + x13 + " = " + t1);
            Console.WriteLine(x21 + "  " + x22 + "  " + x23 + " = " + t2);
            Console.WriteLine(x31 + "  " + x32 + " X33 = T3 ");
            Console.WriteLine("Ingresa el valor X33");
            double x33 = double.Parse(Console.ReadLine());
            Console.Clear();

            Console.WriteLine(x11 + "  " + x12 + "  " + x13 + " = " + t1);
            Console.WriteLine(x21 + "  " + x22 + "  " + x23 + " = " + t2);
            Console.WriteLine(x31 + "  " + x32 + "  " + x33 + " = T3 ");
            Console.WriteLine("Ingresa el valor T3");
            double t3 = double.Parse(Console.ReadLine());
            Console.Clear();



            Console.Clear();
            Console.WriteLine(x11 + "x + " + +x12 + "y + " + x13 + "z = " + t1);
            Console.WriteLine(x21 + "x + " + +x22 + "y + " + x23 + "z = " + t2);
            Console.WriteLine(x31 + "x + " + +x32 + "y + " + x33 + "z = " + t3);
            Console.WriteLine();
            Console.WriteLine(x11 + "  " + x12 + "  " + x13 + " = " + t1);
            Console.WriteLine(x21 + "  " + x22 + "  " + x23 + " = " + t2);
            Console.WriteLine(x31 + "  " + x32 + "  " + x33 + " = " + t3);
           
            //Operaciones de Delta
            double Ds = (x11 * x22 * x33 + x21 * x32 * x13 + x31 * x12 * x23); 
            double Delta = (x13 * x22 * x31 + x23 * x32 * x11 + x33 * x12 * x21);
            Delta = Ds - Delta;
            Console.WriteLine("Delta = " + Delta);
            Console.WriteLine();
            Console.WriteLine("Presiona Enter para continuar");
            Console.ReadLine();
            //Operaciones de X
            double Dx = (t1 * x22 * x33 + t2 * x32 * x13 + t3 * x12 * x23); 
            double X =(x13 * x22 * t3 + x23 * x32 * t1 + x33 * x12 * t2);
            X = Dx - X;
            X = X / Ds;
            Console.WriteLine("x = " + X);
            Console.WriteLine();
            Console.WriteLine("Presiona Enter para continuar");
            Console.ReadLine();
            //Operaciones de Y
            double Dy = (x11 * t2 * x33 + x21 * t3 * x13 + x31 * t1 * x23);
            double Y =(x13 * t2 * x31 + x23 * t3 * x11 + x33 * t1 * x21);
            Y = Dy - Y;
            Y = Y / Ds;
            Console.WriteLine("y = " + Y);
            Console.WriteLine();
            Console.WriteLine("Presiona Enter para continuar");
            Console.ReadLine();
            //Operaciones de Z
            double Dz = (x11 * x22 * t3 + x21 * x32 * t1 + x31 * x12 * t2);
            double Z =(t1 * x22 * x31 + t2 * x32 * x11 + t3 * x12 * x21);
            Z = Dz - Z;
            Z = (Z / Ds);
            Console.WriteLine("z = " + Z);
            Console.WriteLine();
            Console.WriteLine("Presiona Enter para continuar");
            Console.ReadLine();
        }
    }
}
