package football;

public abstract class game_area {
	
	public abstract void players();
	public abstract void record();
	public abstract void goal();
	public abstract void officers();
	public abstract void game_Time();
	public abstract void action_Play();
}
